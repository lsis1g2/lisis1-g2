#include <iostream>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
//teste
using namespace std;

void main() {
	sql::mysql::MySQL_Driver *driver;
	sql::Connection	*con;
	sql::Statement	*stmt;
	sql::ResultSet	*res;

	driver = sql::mysql::get_driver_instance();
	con = driver->connect("tcp://127.0.0.1:3306", "root", "");

	stmt = con->createStatement();
	//stmt->execute("Create database LSIS1");
	stmt->execute("Use LSIS1");
	//stmt->execute("Create table equipas(nome varchar(30), numero int(30) primary key, grupo int(5), Primary Key(numero))");
	//stmt->execute("Create table configuracao_robo(modelo varchar(20), materiais varchar(20), provas int(20), grupo int(5) Primary Key)");
	//stmt->execute("Alter table equipa add foreign key (grupo) references configuracao_robo(grupo)");
	//stmt->execute("insert into configuracao_robo(modelo,materiais,provas,grupo) values ('Lagartas','Muitos',100,2)");
	//stmt->execute("insert into equipas(nome, numero, grupo) values ('Andre Carvalho', 1131171,2), ('David Teixeira',1140259,2),('William Osorio', 1140584,2),('Jose Marques Moura',1140267,2),('Fabio Miranda',1130847,2)");
	
	
	
	res = stmt->executeQuery("Select * from equipas");
	while (res->next()) {
		cout << "Nome: " << res->getString("nome") << " Numero: " << res->getInt("numero") << " Grupo: " << res->getInt("grupo") << endl; 
	}
	delete stmt;
	delete con;
	delete res;
	
	cin.get();

}