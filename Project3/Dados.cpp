#include <iostream>
#include <string>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include "Menu.h"
#include "Dados.h"
using namespace sql;
using namespace std;
Menu m;
Dados::Dados() {

	try {
		driver = mysql::get_driver_instance();
		con = driver->connect("tcp://127.0.0.1:3306", "root", "");
		con->setSchema("lsis1");
	}
	catch (SQLException &e){
		cout << "Erro: " << e.what() << endl;
	}
};

void Dados::visualizarGrupos() {
	
	stmt = con->createStatement();
	res = stmt->executeQuery("Select * from elementos");
	while (res->next()) {
		cout << "NOME: " << res->getString("nome") << " NUMERO: " << res->getInt("numero") << " GRUPO: " << res->getInt("grupo") << "\n" << endl;
	};
};
void Dados::visualizarConfiguracoes() {
	stmt = con->createStatement();
	res = stmt->executeQuery("Select * from configuracao_robo");
	while (res->next()) {
		cout << "MODELO: " << res->getString("modelo") << " MATERIAIS: " << res->getString("materiais") << " GRUPO: " << res->getInt("grupo") << " PARTICIPACOES: " << res->getString("participacoes") << " MELHOR TEMPO: " << res->getInt("melhorTempo") << "\n" <<endl;
	};
};
void Dados::inserirGrupo(string grupo) {
	stmt = con->createStatement();
	stmt->execute("insert into grupos(numero) values(" + grupo + ")");
}
void Dados::inserirDadosEquipas(string nome,string numero, string grupo) {
	stmt = con->createStatement();
	stmt->execute("insert into elementos(nome, numero, grupo) values ('"+nome+"',"+numero+",'"+grupo+"')");
};

void Dados::alterarMembros(string numero) {
	stmt = con->createStatement();
	res = stmt->executeQuery("Select numero from elementos");
	saida = false;
	while (res->next()) {
		if (numero == res->getString("numero")) {
			saida = true;
		}
	}
	if (saida == true) {
		cout << "1- Elimina membro\n2- Alterar atributos" << endl;
		cin >> y;
		if (y == 1) {
			stmt->execute("delete from elementos where numero ='" + numero + "'");
		}
		else if (y == 2) {
			cout << "Nome: " << endl;
			cin >> ws;
			getline(cin, tempNome);
			cout << "Numero: " << endl;
			cin >> tempNumero;
			cout << "Grupo: " << endl;
			cin >> tempGrupo;

			stmt->execute("delete from elementos where numero ='" + numero + "'");
			stmt->execute("insert into elementos(nome, numero, grupo) values('" + tempNome + "','" + tempNumero + "','" + tempGrupo + "')");
		}
		else {
			cout << "VALOR INVALIDO!" << endl;
			m.Opcoes();
		};
	}
	else{
		cout << "O numero que introduziu nao existe." << endl;
		m.Opcoes();
	};
};

void Dados::eliminarGrupos(string grupo) {
	stmt = con->createStatement();
	stmt->execute("delete from elementos where grupo = '" + grupo + "'");
};

void Dados::inserirConfiguracao(string modelo, string materiais, string participacoes, string grupo, string melhorTempo) {
	stmt = con->createStatement();
	stmt->execute("insert into configuracao_robo(modelo,materiais,participacoes,grupo,melhorTempo) values ('" + modelo + "','" + materiais + "','" + participacoes + "',"+grupo+"," + melhorTempo + ")");
	};

void Dados::alterarConfiguracao(string grupo) {
	stmt = con->createStatement();
	res = stmt->executeQuery("Select grupo from configuracao_robo");
	saida = false;
	while (res->next()) {
		if (grupo == res->getString("grupo")) {
			saida = true;
		}
	}
	if (saida == true) {
		cout << "1- Remover Configuracao\n2- Alterar Configuracao" << endl;
		cin >> y;
		if (y == 1) {
			stmt->execute("delete from configuracao_robo where grupo ='" + grupo + "'");
		}
		if (y == 2) {
			cout << "Modelo:" << endl;
			cin >> tempModelo;
			cout << "Materiais:" << endl;
			cin >> tempMateriais;
			cout << "Grupo:" << endl;
			cin >> tempGrupo;
			cout << "Participacoes" << endl;
			cin >> tempParticipacoes;
			cout << "Melhor Tempo:";
			cin >> tempMelhorTempo;

			stmt->execute("delete from configuracao_robo where grupo = '" + grupo + "'");
			stmt->execute("insert into configuracao_robo(modelo,materiais,grupo,participacoes,melhorTempo) values ('" + tempModelo + "','" + tempMateriais + "','" + tempGrupo + "','" + tempParticipacoes + "','" + tempMelhorTempo + "')");
		}
		else {
			cout << "Valor Invalido" << endl;
			m.Opcoes();
		}
	};
}

void Dados::fecharLigacao() {
	con->close();
	delete con;
	delete driver;
	delete stmt;
	delete res;
}