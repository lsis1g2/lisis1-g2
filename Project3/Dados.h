#ifndef DADOS_H
#define DADOS_H
#include <iostream>
#include <stdlib.h>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include "Menu.h"

using namespace std;
using namespace sql;

class Dados {
private:
	int y;
	mysql::MySQL_Driver *driver;
	Connection	*con;
	Statement	*stmt;
	ResultSet	*res;
	string tempNome;
	string tempNumero;
	string tempGrupo;
	string tempModelo;
	string tempMateriais;
	string tempParticipacoes;
	string tempMelhorTempo;
	bool saida;
public:
	Dados();
	void inserirDadosEquipas(string nome, string numero, string grupo);
	void alterarMembros(string numero);
	void visualizarGrupos();
	void eliminarGrupos(string grupo);
	void inserirConfiguracao(string modelo, string materiais, string participacoes,string grupo, string melhorTempo);
	void visualizarConfiguracoes();
	void alterarConfiguracao(string numero);
	void fecharLigacao();
	void inserirGrupo(string grupo);
};



#endif // !DADOS_H
