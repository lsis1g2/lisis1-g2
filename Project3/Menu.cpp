#include <iostream>
#include <stdlib.h>
#include "Menu.h"
#include "Dados.h"


using namespace std;
using namespace sql;

Menu::Menu() {};

void Menu::Opcoes() {

	cout << "1- Visualizar elementos\n2- Visualizar Configuracoes\n3- Registar elemento\n4- Alterar membros\n5- Eliminar Grupos\n6- Registar configuracao do robo\n7- Alterar configuracao robo\n8- Inserir Grupo\n\n0- Sair" << endl;
		cin >> x;

		switch (x) {
		case 1:
			d.visualizarGrupos();
			break;
		case 2:
			d.visualizarConfiguracoes();
			break;
		case 3:
			cout << "Nome: " << endl;
			cin >> ws;
			getline(cin, nome);
			cout << "Numero: " << endl;
			cin >> numero;
			cout << "Grupo: " << endl;
			cin >> grupo;
			d.inserirDadosEquipas(nome, numero, grupo);
			break;
		case 4:
			cout << "Numero:" << endl;
			cin >> numero;
			d.alterarMembros(numero);
			break;
		case 5:
			cout << "Grupo: " << endl;
			cin >> grupo;
			d.eliminarGrupos(grupo);
			break;
		case 6: 
			cout << "Modelo:" << endl;
			cin >> modelo;
			cout << "Materiais:" << endl;
			cin >> materiais;
			cout << "Participacoes:" << endl;
			cin >> participacoes;
			cout << "Grupo:" << endl;
			cin >> grupo;
			cout << "Melhor Tempo:" << endl;
			cin >> melhorTempo;
			d.inserirConfiguracao(modelo, materiais, participacoes,grupo, melhorTempo);
				break;
		case 7:
			cout << "Numero do grupo:" << endl;
			cin >> grupo;
			d.alterarConfiguracao(grupo);
 			break;
		case 8:
			cout << "Numero:" << endl;
			cin >> grupo;
			d.inserirGrupo(grupo);
			break;
		case 0:
			d.fecharLigacao();
			exit(0);
		default:
			cout << "Valor introduzido invalido" << endl;
			Opcoes();
			break;
		}
		Opcoes();
};