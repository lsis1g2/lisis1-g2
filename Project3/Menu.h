#ifndef MENU_H
#define MENU_H
#include "Dados.h"
#include <iostream>
#include <stdlib.h>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>

using namespace std;
using namespace sql;

class Menu {

public:
	string nome;
	string grupo;
	string numero;
	string modelo;
	string materiais;
	string participacoes;
	string melhorTempo;
	Menu();
	void Opcoes();
	int x;
	Dados d;

};

#endif 
